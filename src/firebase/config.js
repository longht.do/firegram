import  firebase from 'firebase/app';
import 'firebase/storage';
import 'firebase/firestore';

// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: "AIzaSyALILsBVxnk8h5XSYNN2GANLtlaR11VLpM",
  authDomain: "firegram-6256f.firebaseapp.com",
  projectId: "firegram-6256f",
  storageBucket: "firegram-6256f.appspot.com",
  messagingSenderId: "311384470453",
  appId: "1:311384470453:web:21cf92e7b5f31ee71b1ffc"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

const projectStorage = firebase.storage();
const projectFireStore = firebase.firestore();
const timestamp = firebase.firestore.FieldValue.serverTimestamp;

export { projectStorage, projectFireStore, timestamp };