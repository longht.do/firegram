import React from 'react'

const Title = () => {
  return (
    <div className='title'>
      <h1>Firegram</h1>
      <h2>Your Pictures</h2>
      <p>Lorem ipsum dummy text</p>
    </div>
  )
}

export default Title
