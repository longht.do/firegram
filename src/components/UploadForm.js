import React, { useState } from 'react'
import ProgressBar from './ProgressBar';

 const UploadForm = () => {
  const [file, setFile] = useState(null);
  const [err, setErr] = useState(null);

  const types = ['image/jpg', 'image/png', 'image/jpeg'];

  const onChangeHandler = (e) => {
     let selected = e.target.files[0]

     if(selected && types.includes(selected.type)) {
       setFile(selected);
       setErr('');
     } else {
       setFile(null);
       setErr('Please choose another file (jpeg and png only)');
     }
  }

  return (
    <form>
      <input type='file' onChange={onChangeHandler}  />
      <div className='output'>
        { err && <div className='err'>{ err }</div> }
        { file && <div>{ file.name }</div>}
        { file && <ProgressBar file={file} setFile={setFile} />}
      </div>
    </form>
  )
 }

 export default UploadForm

